import os
import json
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.options import Options

class Craigslist:
  def post_ad(self, logins, center, post_type):
    email = logins[center]['login']
    password = logins[center]['password']

    # set title of ad
    with open("json/titles.json") as title_file:
      titles = json.load(title_file)

    # set html file for body of post
    html_file = "html/" + post_type + ".html"
    with open (html_file) as body_html:
      body = body_html.read()

    # set json file for general info
    json_file = "json/" + center + ".json"

    with open(json_file) as data_file:
      craigslist_post = json.load(data_file)

    title = titles[post_type].format(craigslist_post['location'])

    image_file = "json/" + post_type + "_image.json"

    with open(image_file) as img_file:
      image_json = json.load(img_file)

    options = Options()
    options.set_headless(headless=True)
    driver = webdriver.Chrome(chrome_options=options)
    # driver.implicitly_wait(10)

    # open craigslist
    driver.get('https://minneapolis.craigslist.org/')
    driver.implicitly_wait(10)

    # go to sign in page and log-in then return to home
    my_account_link = "https://accounts.craigslist.org/login/home"
    driver.get(my_account_link)
    driver.implicitly_wait(10)
    email_box = driver.find_element_by_css_selector('#inputEmailHandle')
    password_box = driver.find_element_by_css_selector('#inputPassword')
    log_in_button = driver.find_element_by_css_selector('body > section > section > div > div.accountform.login-box > form > div.accountform-actions > button')
    email_box.send_keys(email)
    password_box.send_keys(password)
    log_in_button.click()
    driver.implicitly_wait(10)
    craigslist_home = driver.find_element_by_css_selector('body > article > header > a:nth-child(2)')
    craigslist_home.click()
    driver.implicitly_wait(10)

    # start posting process
    post_to_classifieds = driver.find_element_by_css_selector('#post')
    post_to_classifieds.click()
    driver.implicitly_wait(10)

    # select all basic ad settings (location, housing offered, office/commercial)
    if(json_file == "json/woc.json"):
      location_selector = driver.find_element_by_css_selector('body > article > section > form > ul > li:nth-child(4) > label > input[type="radio"]')
    else:
      location_selector = driver.find_element_by_css_selector('body > article > section > form > ul > li:nth-child(1) > label > input[type="radio"]')
    location_selector.click()
    driver.implicitly_wait(10)
    housing_offered = driver.find_element_by_css_selector('body > article > section > form > ul > li:nth-child(4) > label > span.left-side > input[type="radio"]')
    housing_offered.click()
    driver.implicitly_wait(10)
    # #new-edit > div > label > label:nth-child(4) > input
    office_commercial = driver.find_element_by_css_selector(
        '#new-edit > div > label > label:nth-child(4) > input')
    office_commercial.click()
    driver.implicitly_wait(10)

    # fill in add info then continue to pictures
    driver.find_element_by_css_selector('#PostingTitle').send_keys(title)
    driver.find_element_by_css_selector('#GeographicArea').send_keys(craigslist_post['location'])
    driver.find_element_by_css_selector('#postal_code').send_keys(craigslist_post['zip'])
    posting_body = driver.find_element_by_css_selector('#PostingBody')
    if(html_file == 'html/cowork.html'):
      posting_body.send_keys(body.format(craigslist_post['location'], craigslist_post['oc'], craigslist_post['address']))
    elif(html_file == 'html/office.html'):
      posting_body.send_keys(body.format(craigslist_post['location'], craigslist_post['oc'], craigslist_post['address']))
    else:
      posting_body.send_keys(body)
    driver.find_element_by_css_selector('#new-edit > div > fieldset.json-form-group-container.contact-info > div > fieldset > div > div.json-form-group.json-form-group-container.contact-form-booleans > label.json-form-item.boolean.contact_phone_ok > input').click()
    driver.find_element_by_css_selector('#new-edit > div > fieldset.json-form-group-container.contact-info > div > fieldset > div > div.json-form-group.json-form-group-container.contact-form-text > label.json-form-item.text.contact_phone > label > input').send_keys(craigslist_post['phone_number'])
    driver.find_element_by_css_selector('#new-edit > div > fieldset.json-form-group-container.location-info > div > label.json-form-item.text.xstreet0.street0 > label > input').send_keys(craigslist_post['address'])
    driver.find_element_by_css_selector('#new-edit > div > fieldset.json-form-group-container.location-info > div > label.json-form-item.text.city > label > input').send_keys(craigslist_post['location'])
    driver.find_element_by_css_selector('#new-edit > div > div.json-form-group.json-form-group-container.submit-buttons > div > button').click()
    driver.implicitly_wait(10)
    driver.find_element_by_css_selector('#leafletForm > button').click()
    driver.implicitly_wait(10)

    # start image process
    driver.find_element_by_css_selector("#classic").click()
    driver.implicitly_wait(5)
    for i in image_json[center]:
      image_box = driver.find_element_by_css_selector('#uploader > form > input[type="file"]:nth-child(3)')
      image_box.send_keys(i)

    driver.find_element_by_css_selector("body > article > section > form > button").click()
    driver.find_element_by_css_selector("#publish_top > button").click()

    print("{} Ad Posted.".format(post_type))
    driver.quit()

  def renew_posts(self, logins, center):
    email = logins[center]['login']
    password = logins[center]['password']
    options = Options()
    options.set_headless(headless=True)
    driver = webdriver.Firefox(firefox_options=options)
    my_account_link = "https://accounts.craigslist.org/login/home"
    driver.get(my_account_link)
    driver.find_element_by_css_selector("#inputEmailHandle").send_keys(email)
    driver.find_element_by_css_selector("#inputPassword").send_keys(password)
    driver.find_element_by_css_selector(
        "body > section > section > div > div.accountform.login-box > form > div.accountform-actions > button"
    ).click()
    driver.implicitly_wait(5)
    accounts_url = driver.current_url
    # print(accounts_url)
    posts = driver.find_elements_by_css_selector('#paginator > table > tbody > *')
    print("Current Ads Count: " + str(len(posts)))
    # #paginator > table > tbody > tr:nth-child(1)
    # #paginator > table > tbody > tr:nth-child(6) > td.buttons.active > div > form:nth-child(4) > input.managebtn
    count = 0
    for x in range(len(posts)):
      try:
        post = driver.find_element_by_css_selector('#paginator > table > tbody > tr:nth-child({0}) > td.buttons.active > div > form:nth-child(4) > input.managebtn'.format(x+1))
        driver.implicitly_wait(5)
        post.click()
        print("{0}. Renewed".format(x+1))
        count += 1
        driver.get(accounts_url)
        driver.implicitly_wait(10)
      except NoSuchElementException:
        print("{0}. Not Renewable".format(x+1))

    print('\nProcess Completed: {0} Ads Reposted'.format(count))

  def log_in(self, logins, center):
    print("Logging in to {}.".format(center))
    email = logins[center]['login']
    password = logins[center]['password']
    driver = webdriver.Safari()
    driver.get("https://accounts.craigslist.org/login/home")
    email_box = driver.find_element_by_css_selector('#inputEmailHandle')
    password_box = driver.find_element_by_css_selector('#inputPassword')
    log_in_button = driver.find_element_by_css_selector('body > section > section > div > div.accountform.login-box > form > div.accountform-actions > button')
    email_box.send_keys(email)
    password_box.send_keys(password)
    log_in_button.click()

  def post_all(self, logins):
    for center in logins:
      print("Posting ads for {}.".format(center))
      self.post_ad(logins, center, "office")
      self.post_ad(logins, center, "cowork")
      self.post_ad(logins, center, "conference")
      print("{} posting completed.".format(center))

    print("All ads posted.")
  
  def post_all_office(self, logins):
    for center in logins:
      print("Posting ads for {}.".format(center))
      self.post_ad(logins, center, "office")
      print("{} posting completed.".format(center))

    print("All ads posted.")

  def post_all_cowork(self, logins):
    for center in logins:
      print("Posting ads for {}.".format(center))
      self.post_ad(logins, center, "cowork")
      print("{} posting completed.".format(center))

    print("All ads posted.")

  def post_individual(self, logins, center):
    options = {
      "1": "office",
      "2": "cowork",
      "3": "conference",
      "4": "all",
      "5": "exit"
    }
    for key,val in options.items():
      print("{}. {}".format(key,val))
    choice = options.get(input("--> "), "none")
    if (choice == "office"):
      print("Posting office ad for {}.".format(center))
      self.post_ad(logins, center, "office")
      print("{} posting completed.".format(center))
    elif (choice == "cowork"):
      print("Posting cowork ad for {}.".format(center))
      self.post_ad(logins, center, "cowork")
      print("{} posting completed.".format(center))
    elif (choice == "conference"):
      print("Posting conference ad for {}.".format(center))
      self.post_ad(logins, center, "conference")
      print("{} posting completed.".format(center))
    elif (choice == "all"):
      print("Posting ads for {}.".format(center))
      self.post_ad(logins, center, "office")
      self.post_ad(logins, center, "cowork")
      self.post_ad(logins, center, "conference")
      print("{} posting completed.".format(center))
    else:
      print ("Improper Input. Returning to Menu.")

  def renew_all(self, logins):
    for center in logins:
      print("Renewing posts for {}.".format(center))
      self.renew_posts(logins, center)
    print("All available reposted")


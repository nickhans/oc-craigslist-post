import os
import json
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

post_dict = {
  "1" : "cowork",
  "2" : "office",
  "3" : "conference"
}

location_dict = {
  "1" : "foc",
  "2" : "moc",
  "3" : "ocip",
  "4" : "poc",
  "5" : "upoc",
  "6" : "woc"
}

def choose(d):
  for key, val in d.items():
    print("{}. {}".format(key, val))

  return d.get(input("Choose Option: "), "none")

# log-in info
login_file = "json/login_info.json"

with open (login_file) as login_json:
  login = json.load(login_json)

email = login['email']
password = login['password']

# set title of ad
title = input("Ad Title: ")

post_type = "none"

while post_type is "none":
  post_type = choose(post_dict)

# set html file for body of post
html_file = "html/" + post_type + ".html"

with open (html_file) as body_html:
  body = body_html.read()

location = "none"

while location is "none":
  location = choose(location_dict)

# set json file for general info
json_file = "json/" + location + ".json"

with open(json_file) as data_file:
  craigslist_post = json.load(data_file)

image_file = "json/" + post_type + "_image.json"

with open(image_file) as img_file:
  image_json = json.load(img_file)

options = Options()
options.set_headless(headless=True)
driver = webdriver.Chrome(chrome_options=options)
# driver.implicitly_wait(10)

# open craigslist
driver.get('https://minneapolis.craigslist.org/')
driver.implicitly_wait(10)

# go to sign in page and log-in then return to home
my_account_link = "https://accounts.craigslist.org/login/home"
driver.get(my_account_link)
driver.implicitly_wait(10)
email_box = driver.find_element_by_css_selector('#inputEmailHandle')
password_box = driver.find_element_by_css_selector('#inputPassword')
log_in_button = driver.find_element_by_css_selector('body > section > section > div > div.accountform.login-box > form > div.accountform-actions > button')
email_box.send_keys(email)
password_box.send_keys(password)
log_in_button.click()
driver.implicitly_wait(10)
craigslist_home = driver.find_element_by_css_selector('body > article > header > a:nth-child(2)')
craigslist_home.click()
driver.implicitly_wait(10)

# start posting process
post_to_classifieds = driver.find_element_by_css_selector('#post')
post_to_classifieds.click()
driver.implicitly_wait(10)

# select all basic ad settings (location, housing offered, office/commercial)
if(json_file == "json/woc.json"):
  location_selector = driver.find_element_by_css_selector('body > article > section > form > ul > li:nth-child(4) > label > input[type="radio"]')
else:
  location_selector = driver.find_element_by_css_selector('body > article > section > form > ul > li:nth-child(1) > label > input[type="radio"]')
location_selector.click()
driver.implicitly_wait(10)
housing_offered = driver.find_element_by_css_selector('body > article > section > form > ul > li:nth-child(4) > label > span.left-side > input[type="radio"]')
housing_offered.click()
driver.implicitly_wait(10)
# #new-edit > div > label > label:nth-child(4) > input
office_commercial = driver.find_element_by_css_selector(
    '#new-edit > div > label > label:nth-child(4) > input')
office_commercial.click()
driver.implicitly_wait(10)

# fill in add info then continue to pictures
driver.find_element_by_css_selector('#PostingTitle').send_keys(title)
driver.find_element_by_css_selector('#GeographicArea').send_keys(craigslist_post['location'])
driver.find_element_by_css_selector('#postal_code').send_keys(craigslist_post['zip'])
posting_body = driver.find_element_by_css_selector('#PostingBody')
if(html_file == 'html/cowork.html'):
  posting_body.send_keys(body.format(craigslist_post['location'], craigslist_post['oc'], craigslist_post['address']))
elif(html_file == 'html/office.html'):
  posting_body.send_keys(body.format(craigslist_post['location'], craigslist_post['oc'], craigslist_post['address']))
else:
  posting_body.send_keys(body)
driver.find_element_by_css_selector('#new-edit > div > fieldset.json-form-group-container.contact-info > div > fieldset > div > div.json-form-group.json-form-group-container.contact-form-booleans > label.json-form-item.boolean.contact_phone_ok > input').click()
driver.find_element_by_css_selector('#new-edit > div > fieldset.json-form-group-container.contact-info > div > fieldset > div > div.json-form-group.json-form-group-container.contact-form-text > label.json-form-item.text.contact_phone > label > input').send_keys(craigslist_post['phone_number'])
driver.find_element_by_css_selector('#new-edit > div > fieldset.json-form-group-container.location-info > div > label.json-form-item.text.xstreet0.street0 > label > input').send_keys(craigslist_post['address'])
driver.find_element_by_css_selector('#new-edit > div > fieldset.json-form-group-container.location-info > div > label.json-form-item.text.city > label > input').send_keys(craigslist_post['location'])
driver.find_element_by_css_selector('#new-edit > div > div.json-form-group.json-form-group-container.submit-buttons > div > button').click()
driver.implicitly_wait(10)
driver.find_element_by_css_selector('#leafletForm > button').click()
driver.implicitly_wait(10)

# start image process
driver.find_element_by_css_selector("#classic").click()
driver.implicitly_wait(5)
for i in image_json[location]:
  image_box = driver.find_element_by_css_selector('#uploader > form > input[type="file"]:nth-child(3)')
  image_box.send_keys(i)

driver.find_element_by_css_selector("body > article > section > form > button").click()
driver.find_element_by_css_selector("#publish_top > button").click()

print("Process Complete..")

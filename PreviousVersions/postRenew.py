from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.options import Options
import json


def get_login_info(login_file):
    # log-in info
    with open(login_file) as login_json:
        login = json.load(login_json)

    email = login["email"]
    password = login["password"]
    return email, password


def main():
    login_file = "json/login_info.json"
    email, password = get_login_info(login_file)
    options = Options()
    options.set_headless(headless=True)
    driver = webdriver.Firefox(firefox_options=options)
    my_account_link = "https://accounts.craigslist.org/login/home"
    driver.get(my_account_link)
    driver.find_element_by_css_selector("#inputEmailHandle").send_keys(email)
    driver.find_element_by_css_selector("#inputPassword").send_keys(password)
    driver.find_element_by_css_selector(
        "body > section > section > div > div.accountform.login-box > form > div.accountform-actions > button"
    ).click()
    driver.implicitly_wait(5)
    accounts_url = driver.current_url
    # print(accounts_url)
    posts = driver.find_elements_by_css_selector('#paginator > table > tbody > *')
    print("Current Ads Count: " + str(len(posts)))
    # #paginator > table > tbody > tr:nth-child(1)
    # #paginator > table > tbody > tr:nth-child(6) > td.buttons.active > div > form:nth-child(4) > input.managebtn
    count = 0
    for x in range(len(posts)):
      try:
        post = driver.find_element_by_css_selector('#paginator > table > tbody > tr:nth-child({0}) > td.buttons.active > div > form:nth-child(4) > input.managebtn'.format(x+1))
        driver.implicitly_wait(5)
        post.click()
        print("{0}. Renewed".format(x+1))
        count += 1
        driver.get(accounts_url)
        driver.implicitly_wait(10)
      except NoSuchElementException:
        print("{0}. Not Renewable".format(x+1))

    print('\nProcess Completed: {0} Ads Reposted'.format(count))

# executes main
main()


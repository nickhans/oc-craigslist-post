from OC_Craigslist import Craigslist
import json

def main():
  craig = Craigslist()

  with open("json/login_info.json") as login_file:
    logins = json.load(login_file)

  location_dict = {
    "1" : "foc",
    "2" : "moc",
    "3" : "ocip",
    "4" : "poc",
    "5" : "upoc",
    "6" : "woc"
  }

  is_exit = False

  while not is_exit:
    print("1. Renew All\n2. Post All\n3. Post Individual\n4. Log-In\n5. Quit")
    option = input("--> ")
    if (option == '1'):
      craig.renew_all(logins)
    elif (option == '2'):
      all_option = {
        "1": "All",
        "2": "Office",
        "3": "Cowork"
      }
      for key,val in all_option.items():
        print("{}. {}".format(key,val))
      all_choice = all_option.get(input("--> "), "none")
      if (all_choice is not "none"):
        if (all_choice == "All"):
          craig.post_all(logins)
        elif (all_choice == "Office"):
          craig.post_all_office(logins)
        elif (all_choice == "Cowork"):
          craig.post_all_cowork(logins)
      else:
        print ("Improper Input.")
    elif (option == '3'):
      for key, val in location_dict.items():
        print("{}. {}".format(key, val))
      center = location_dict.get(input("--> "), "none")
      if (center is not 'none'):
        craig.post_individual(logins, center)
      else:
        print("Improper Input.")
    elif (option == '4'):
      for key, val in location_dict.items():
        print("{}. {}".format(key, val))
      center = location_dict.get(input("--> "), "none")
      if (center is not 'none'):
        craig.log_in(logins, center)
      else:
        print("Improper Input.")
    elif (option == '5'):
      print("Exiting...")
      is_exit = True
    else:
      print("Improper Input, Please Try Again.")

main()

